<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Description of home_doc
 *
 * @author Gebruiker
 */
class HomeDoc extends BasicDoc{
    public function __construct($model) {
        parent::__construct($model);
    }
    
   protected function mainContent(){
   echo  '<p>
      Fijn dat u onze website hebt gevonden.<br>
      Hier kunt u informatie vinden over <a href="index.php?page=about">wat wij doen en waar u ons kunt vinden.</a><br>
      <a href="index.php?page=contact">Maar ook hoe u contact op kan nemen,</a>
      <a href="index.php?page=webshop">en wat u bij ons kan kopen.</a>
    </p>';
   }
}
