<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_controller
 *
 * @author Gebruiker
 */
class UserController extends PageController{
  public function __construct() {
    parent::__construct();
  }
    
  //===============================  
  public function processRegister(){
    require_once 'user_model.php';
    $this->model = new UserModel($this->model);
    if($this->model->toValidate()){
      $this->model->validateRegisterData();
    }
    if($this->model->isValid()) {
      $this->model->storeUser();
      $this->model->setPage('login');
    } else {
      $this->model->setPage('register');
    }
    return $this->model;
  }
  //===============================
  public function processLogin(){
    require_once 'user_model.php';
    $this->model = new UserModel($this->model);
    if($this->model->toValidate()){
      $this->model->validateLoginData();
    }
    if($this->model->isValid()) {
      $this->model->logIn();
      $this->model->setPage('home');
    } else {
      $this->model->setPage('login');
    }
    return $this->model;
  }
  //===============================
  public function processContact(){
    require_once 'user_model.php';
    $this->model = new UserModel($this->model);
    if($this->model->toValidate()){
      $this->model->validateContactData();
    }
    $this->model->setPage('contact');
    return $this->model;
  }
  //=================================
  public function processCheckout(){
    require_once 'checkout_model.php';
    $this->model= new CheckoutModel($this->model);
    $this->model->setPage('checkout');
    if($this->model->toValidate()){
      $this->model->validateCheckout();
      if($this->model->isValid()){
        $this->model->confirmOrder();
        $this->model->setPage('confirm');
      }
    }
    return $this->model;
  }
}
