<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'form_doc.php';
/**
 * Description of item_doc
 *
 * @author Gebruiker
 */
class ItemDoc extends FormDoc{
  public function __construct($model) {
    parent::__construct($model);
  }
  
  protected function title() {
    echo "<title>Opdracht_4.2 - " .$this->model->getItemData()['name']. "</title>";
    
  }
  public function mainContent() {
    $this->showItemContent($this->model->getItemData());
    if($this->model->isAllowedToBuy()==true){
      $this->formFields();
    }
    echo '</div>';
  }
  protected function bodyHeader() {
    echo '<div class="jumbotron d-none d-md-block">'
    . '<h1>'.$this->model->getItemData()['name'].'</h1>'.PHP_EOL
    . '</div>'.PHP_EOL; 
  }
  
  protected function formFields(){  
    $min = $this->model->getItemData()['min'];
    $max = $this->model->getItemData()['max'];
    parent::beginForm();
    parent::hiddenFormInput('webshop', 'page');
    parent::hiddenFormInput($this->model->getItemData()['product_id'], 'product_id');
    parent::showFormInput('amount', 'aantal', 'number','',0,0,'required', $min, $max, 3, 3);
    parent::formEnd('submit', 'Kopen');
  }
    
  private function showItemContent($data){
    $this->displayHeader($data['name']);
    $this->showImage($data['image_src']);
    $this->showDescription($data['description']);
    echo '<div class="container">';
    $this->showPriceTag($data['price']);
  }
  
  private function displayHeader($name) {
    echo '<div class="d-md-none">'
    . '<h3>'.$name.'</h3>'
    . '</div>';      
  }
  private function showImage($image_src){
    echo '<div class="container"><img class="img-fluid m-auto" src="images/'.$image_src.'"></div>';
  }
  
  private function showDescription($description){
    echo '<div class="container"'
    . '  <h2>Beschrijving:</h2>'
    . '<p>'.PHP_EOL
    . $description
    .'</p>'.PHP_EOL
    .'</div>';
  }
  
  private function showPriceTag($price){
    echo '<h2>Dit product kost: &euro;'.($price/100).'</h2>'.PHP_EOL;
  }
}
