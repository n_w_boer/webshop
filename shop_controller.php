<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'page_controller.php';
require_once 'shop_model.php';
/**
 * Description of shop_controller
 *
 * @author Gebruiker
 */
class ShopController  extends PageController{
  public function __construct($model) {
    parent::__construct();
    $this->model=new ShopModel($model);
  }
  //=================================
  public function processShoppingCart(){
    return $this->model;
  }
  //=================================
  function processWebshop(){
    $this->model->setProductList();
    if($_SERVER['REQUEST_METHOD']=='POST'){
      $this->model->fillCart($this->getPostVar('product_id'), $this->getPostVar('amount'));
      $this->model->setPage('webshop');
    }else {
      $this->model->setPage($this->getUrlVar('page'));
    }
    return $this->model;
  }
  

  //================================= 
  function processTopFive(){
    try{
      $this->model->getFiveMostBought();
    }catch(Exception $e){
        $msg = $e->getMessage();
        echo $msg;
        writeExceptionToFile($e->getMessage(), $e);
    }
    return $this->model;
  } 
  //================================= 
  function processItem(){
    if($_SERVER['REQUEST_METHOD']=='POST'){
      $this->model->fillCart($this->getPostVar('product_id'), $this->getPostVar('amount'));
      $this->model->setPage('webshop');
    }else {
      $this->model->setItemData($this->getUrlVar('id'));
      $this->model->setPage('item');
    }
    return $this->model;
  }
}
