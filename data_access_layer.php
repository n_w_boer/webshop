<?php
  //=================================
  //FUNCTIONS
  //=================================
class DataAccessLayer{
  private $error='';
  
  public function getError(){
    return $this->error;
  }
  
  public function clearError(){
    $this->error = '';
  }
  
  private function createConnection(){
    $conn = mysqli_connect('localhost', 'server_back_end_agent', 'CoAoRxe9UvNIyUQi', 'nielsb_website_users');
    if(!$this->connectionExists($conn)) {
      throw new Exception('Failed to establish a connection.');  /* JH TIP: Zet mysqli_connect_error in het bericht, dat maakt uitzoeken veel makkelijker later */
    }
    return $conn;
  }
  //=================================
  private function connectionExists($conn){
    return $conn != false;
  }
  //=================================
  private function closeConnection($conn){
    mysqli_close($conn);
  }
  //=================================
  public function loginDataCorrect($email,$password){ /* JH TIP: Ik had deze functie in het model verwacht */
    $user = $this->findUserByEmail($email);
      if(strcmp($user['email'],$email)==0&&strcmp($user['password'], $password)==0){
        return ['name'=>$user['name'],'id'=>$user['customer_id']];
      }
    return "";
  }
  //=================================
  public function insertCustomer($email, $name,$password){
    try{
      $conn = $this->createConnection();
      $namesafe = $this->filterMySQL($name, $conn);
      $passwordsafe = $this->filterMySQL($password, $conn);
      $emailsafe = $this->filterMySQL($email, $conn);
      $sql = "INSERT INTO customers (email, name, password)
      VALUES ('".$emailsafe."','".$namesafe."', '".$passwordsafe."');";
      if(!mysqli_query($conn, $sql)){
        throw new Exception('Failed to insert new customer.');
      }
    } catch (Exception $e){
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
    }
  }
  //=================================
  public function findUserByEmail($email){
    return $this->findAllXbyKeyInY($email, 'email', 'customers');
  }
  //=================================
  private function filterMySQL($input, $conn){
    return mysqli_real_escape_string($conn, $input);
  }
  //=================================
  public function listOfProducts(){
    try{
      $conn = $this->createConnection();
      $data = [];
      $sql = "SELECT product_id, name, price, image_src, stock FROM products";
      $result = mysqli_query($conn, $sql);  
    if($result==false){
      throw new Exception('Unable to find the products.');
    } else if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)) {
        $data[$row['product_id']] = array('product_id'=>$row['product_id'],'name' => $row['name'], 'price'=>$row['price'], 'image_src' => $row['image_src'], 'stock'=>$row['stock']);
      }
    } 
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $data;
    }
  }
  //=================================
  public function findAllXbyKeyInY($x, $key, $y){
    try{
      $data=[];
      $conn = $this->createConnection();
      $xsafe = $this->filterMySQL($x, $conn);
      $sql = "SELECT * FROM ".$y." WHERE ".$key."='".$xsafe."';";
      $result = mysqli_query($conn, $sql);
      if($result==false){
        throw new Exception('Unable to find any result.'); /* JH TIP: zet hierin ook de $sql en de mysqli_error, dat maakt het uitzoeken later erg handig */
      } else if(mysqli_num_rows($result)==1){
        $data=mysqli_fetch_assoc($result);
      }
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $data;
    }
  }
  //=================================
  public function getXfromYbyKey($x, $target, $key, $y){ 
    try{
      $data = null;
      $conn = $this->createConnection();
      $xsafe = $this->filterMySQL($x, $conn);
      $sql = "SELECT ".$target." FROM ".$y." WHERE ".$key."='".$xsafe."';";
      $result = mysqli_query($conn, $sql);
      if($result==false){
        throw new Exception('Unable to find a result by string.');
      } else if(mysqli_num_rows($result)>0){
        $data=mysqli_fetch_assoc($result);
      }
    } catch (Exception $e){ 
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $data;
    }
  }
  //=================================
  public function getXfromYbyNumKey($x, $target, $key, $y){  
    try{
      $data = null;
      $conn = $this->createConnection(); 
      $sql = "SELECT ".$target." FROM ".$y." WHERE ".$key."=".$x.";";
      $result = mysqli_query($conn, $sql);
      if($result==false){
        throw new Exception('Unable to find a result by number.');
      } else if(mysqli_num_rows($result)>0){
        $data=mysqli_fetch_assoc($result);
      }
    } catch (Exception $e){ 
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $data;
    }
  }
  //=================================
  public function saveAdress($street,$nr,$nr_addition,$zipcode,$city){
    try{
      $conn = $this->createConnection();
      $streetsafe = $this->filterMySQL($street, $conn);
      $nrsafe = $this->filterMySQL($nr, $conn);
      $nr_additionsafe = $this->filterMySQL($nr_addition, $conn);
      $zipcodesafe = $this->filterMySQL($zipcode, $conn);
      $citysafe = $this->filterMySQL($city, $conn);
      $sql = "INSERT INTO adress (street, number, addition, zipcode, city)
      VALUES ('".$streetsafe."', ".$nrsafe.", '".$nr_additionsafe."', '".$zipcodesafe."', '".$citysafe."');";
      if(!mysqli_query($conn, $sql)){
        $lastID='';
        throw new Exception('Unable to save adress.');
      }else{
        $lastID = mysqli_insert_id($conn);
      }
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $lastID;
    }
  }
  //=================================
  public function saveOrderProduct($date, $orderID, $itemID, $amount, $price){ /* JH: Ik zou createOrder en deze functie samenvoegen en in 1 database connectie doen. Je kunt ook database 'transacties' gebruiken door auto_commit uit te zetten en dan 'mysqli_commit' en 'mysqli_rollback' te gebruiken */
    try{
      $conn = $this->createConnection();
      $sql = "INSERT INTO `order_products` (order_date, order_id_fk, product_id_fk, amount, price)
      VALUES ('".$date."', ".$orderID.", ".$itemID.", ".$amount.", ".($price/100).");";
      if(!mysqli_query($conn, $sql)){
        throw new Exception('Unable to save orderproduct with ID'.$itemID);
      }
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';   
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
    }
  }
  //=================================
  public function updateStock($product_id, $amount){ /* JH: Het is beter database ontwerp om 'state' en 'info' gescheiden te houden en dus een aparte tabel 'stock' te maken */
    try{
      $conn = $this->createConnection();
      $sql = "UPDATE products "
      . "SET stock=stock-".$amount." "
      . "WHERE product_id ='".$product_id."' ";
      if(!mysqli_query($conn, $sql)){
        throw new Exception('Unable to save orderproduct with ID'.$itemID);
      }
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';   
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
    }
  }
  //=================================
  public function saveOrder($userID, $adressID){
    try{
      $lastID = '';
      $conn = $this->createConnection();
      $sql = "INSERT INTO `order` (customer_id_fk, adress_id_fk)
      VALUES (".$userID.", ".$adressID.");";
      if(!mysqli_query($conn, $sql)){
        throw new Exception('Unable to save orderproduct with ID'.$itemID);
      }else{
        $lastID = mysqli_insert_id($conn);
      }
    } catch (Exception $e){ /* JH: Ik zou de exceptie hier niet vangen, maar doorgooien en in het model vangen, dan hoef je ook hier geen $error variabelen te hebben */
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';   
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $lastID;
    }
  }
  //=================================
  public function getTopFive(){
    try{
      $data = [];
      $conn = $this->createConnection();
      $sql = "SELECT products.name, MIN(products.product_id), SUM(order_products.amount) "
          . "FROM order_products "
          . "LEFT JOIN products ON order_products.product_id_fk=products.product_id "
          . "WHERE order_products.order_date>=ADDDATE(CURRENT_DATE, -7) "
          . "GROUP BY products.name "
          . "ORDER BY SUM(order_products.amount) DESC "
          . "LIMIT 5;"; /* JH TIP: je kan achter MIN(products.product_id) zetten "AS min_product_id" en dan verderop refereren als min product_id */ /* JH: Waarom heb je de MIN functie uberhaupt nodig? alle id's zijn toch hetzelfde als je groupeert bij id? (i.p.v. name) */ 
      $result = mysqli_query($conn, $sql);
      if($result==false){
        throw new Exception('Unable to find the top five products.');
      } else if (mysqli_num_rows($result) > 0) {
        $pos = 1;
        while($row = mysqli_fetch_assoc($result)) {
          $data[$pos]=['amount'=>$row['SUM(order_products.amount)'], 'name'=>$row['name'], 'id'=>$row['MIN(products.product_id)']];
          $pos++;
        }
      }
    } catch (Exception $e){
      $this->writeToExceptionLog($e->getMessage(), $e->getTraceAsString());
      $this->error='Er was een storing in de database, probeer het later nog eens';   
    }finally{
      if($this->connectionExists($conn)){
        $this->closeConnection($conn);
      }
      return $data;
    }
  }
  
  private function writeToExceptionLog($exceptionMessage, $exceptionStackTrace){
    $file = fopen("exceptionlog/exceptionlog.txt", 'a');
    $message = 'Error: '.$exceptionMessage.'\n'.$exceptionStackTrace.'\n\n';
    fwrite($file, $message);
    fclose($file);
}
}
  