<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'data_model.php';
/**
 * Description of user_model
 *
 * @author Gebruiker
 */
class UserModel extends DataModel {
  private $email="";
  private $message="";
  private $password="";
  private $password2="";
  protected $errors=[];
  protected $id=0;
  protected $valid=false;
   
  public function __construct($model) {
    parent::__construct($model);
  }
  
  public function toValidate() {
    return $_SERVER['REQUEST_METHOD']=="POST"; /* JH TIP: Sla dit op als variabele 'isPost' in PageModel en doe deze test 1x */
  }  
  
  public function validateContactData(){
    /* JH TIP: Is zou hier dan zetten if (!$this->isPost) { return; } dan hoef je in de controller niet steeds de toValidate aan te roepen */
    $this->name = $this->getData("name");
    $this->email = $this->getData("email");
    $this->message = $this->getData("message");
    if($this->isEmpty($this->name)){
      $this->errors['name'] = "Vul je naam in.";
    }else if(!$this->checkLettersAndSpaces($this->name)){
      $this->errors['name'] = "Voer een naam in zonder cijfers en interpunctie.";
    }
    if($this->isEmpty($this->email)){
      $this->errors['email'] = "Vul je email in.";
    }else if ($this->checkEmail($this->email)) {
      $this->errors['email'] = "Verkeerd email format";
    } 
    if ($this->isEmpty($this->message)) {
      $this->errors['message'] = "bericht is verplicht";
    }        
    if ($this->isEmpty($this->errors)) {
      $this->valid=true;
    }  
  }
  
  public function validateLoginData()
  {
    /* JH TIP: Is zou hier dan zetten if (!$this->isPost) { return; } dan hoef je in de controller niet steeds de toValidate aan te roepen */
    $this->email = $this->getData("email");
    $this->password = $this->getData("password");
    $array = $this->dataAccessLayer->loginDataCorrect($this->email, $this->password);
    if(!$this->isEmpty($this->checkExceptions())){
      $this->errors['database']= $this->checkExceptions();
      $this->clearDatabaseErrors();
    }else if (!$this->isEmpty($array)) {
      $this->valid=true;
      $this->name =$array['name'];
      $this->id=$array['id'];
    } else {
      $this->errors['login'] = "de combinatie van email en wachtwoord is incorrect";
    }
  }
  
  public function validateRegisterData()
  {
    /* JH TIP: Is zou hier dan zetten if (!$this->isPost) { return; } dan hoef je in de controller niet steeds de toValidate aan te roepen */
    $this->name = $this->getData("name");
    $this->email = $this->getData("email");
    $this->password = $this->getData("password");
    if($this->isEmpty($this->name)){
      $this->errors['name'] = "Vul je naam in.";
    } else if(!$this->checkLettersAndSpaces($this->name)){
      $this->errors['name'] = "Je naam bestaat alleen uit letters.";
    }
     
    if ($this->isEmpty($this->email)) {
      $this->errors['email'] = "Verkeerd email format";
    } else if(!$this->checkEmail($this->email)){
      $this->errors['email'] = "Je email is niet leeg.";
    }
    if ($this->isEmpty($this->password)) {
      $this->errors['password'] = "wachtwoord is verplicht";
    } else {
      $this->password2 = $this->getData("password2");
      if (strcmp($this->password, $this->password2)!=0){
        $this->errors['password'] = "wachtwoorden zijn niet gelijk";
      }
    } 
        
    if ($this->isEmpty($this->errors)) {
      $user= $this->dataAccessLayer->findUserByEmail($this->email);
      if(!$this->isEmpty($this->checkExceptions())){
        $this->errors['database']= $this->checkExceptions();
        $this->clearDatabaseErrors();
      }else if($this->isEmpty($user)){
        $this->valid=true;
      }else{
        $this->errors['register']='Er is al een account geregistreerd op dit adres.';
      }
    }
  }
  public function getName(){
    return $this->name;
  }
  
  public function getEmail(){
    return $this->email;
  }
  
  public function getMessage(){
    return $this->message;
  }
  
  public function isValid(){
    return $this->valid;
  }
  
  public function getErrors() {
    return $this->errors;
  }
  
  public function storeUser(){
    $this->dataAccessLayer->insertCustomer($this->email, $this->name, $this->password);
  }
  
  public function logIn() {
    $this->sessionmanager->logUserIn($this->name, $this->id); 
    $this->loggedIn=true; 
  }
  
  public function logOut(){
    $this->sessionmanager->logUserOut();
    $this->loggedIn=false;
  }
  
  //=================================
  //Validation Utility Functions
  //=================================
  protected function checkEmail ($email){
    return filter_var($this->correctData($email),FILTER_VALIDATE_EMAIL);
  }
  //=================================
  protected function isEmpty($var){
    return empty($var);
  }
  //=================================
  protected function checkLettersAndSpaces($var){
    return preg_match("/^[a-zA-Z ]*$/",$var);
  }
  //=================================
    protected function getData($label){
      return $this->correctData($this->getPostVar($label));
  }
  //=================================
  
}
