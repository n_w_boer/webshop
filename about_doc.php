<?php /* JH TIP: Zet de bestanden in folders /models, /controllers, /views etc */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Description of about_doc
 *
 * @author Gebruiker
 */
class AboutDoc extends BasicDoc{
    public function __construct($model) {
        parent::__construct($model);
    }
    
    protected function mainContent()
    {
       echo '<div class="info"> '.PHP_EOL.
      '<h2 class="about">Algemene informatie</h2>'.PHP_EOL
      .'<p>'.PHP_EOL
    .'<strong>Naam:</strong> scoutingwinkel U ook?<br>'.PHP_EOL
    .'<strong>Locatie:</strong> Utrecht<br>'.PHP_EOL
    .'<strong>Straat:</strong> Uw dromen<br>'.PHP_EOL
    .' </p>
    </div>'.PHP_EOL

    .'<div class="info">'.PHP_EOL
    .'  <h2 class="about">Onze missie</h2>'.PHP_EOL
    .'  <p>
      Bent u er klaar mee altijd maar bij andere groepen te moeten vragen naar het scheerlijnenvet?<br>
      Of dat de bosstofzuiger het nooit langer dan een dag volhoudt? <br>
      Vindt u het ook onverantwoord uw oudste verkenners de <i>schop met het voetje</i> te laten halen bij de buren?<br>
     Dan bent u bij ons aan het juiste adres!<br>
      Wij maken het namelijk onze missie u ten alle tijden te kunnen voorzien in alle kampbenodigdheden.
      </p>'.PHP_EOL
    .'</div>'.PHP_EOL

    .'<div class="info">'.PHP_EOL
    .'  <h2 class="about">Wij hebben een ruim assortiment bestaande uit onder andere:</h2>'.PHP_EOL
    .'  <ul>'.PHP_EOL
    .'<li>Scheerlijnenvet</li>'.PHP_EOL
    .'<li>Rubberen haringen</li>'.PHP_EOL
    .'<li>Bosstofzuigers</li>'.PHP_EOL
    .'<li>Middenpalen voor verkennertenten</li>'.PHP_EOL
    .'<li>En nog veel meer</br>'.PHP_EOL
	    
    .'</ul>'.PHP_EOL
    .'</div>';
    } 

}
