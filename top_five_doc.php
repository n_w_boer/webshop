<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Description of top_five_doc
 *
 * @author Gebruiker
 */
class TopFiveDoc extends BasicDoc {
    public function __construct($model) {
        parent::__construct($model);
    }
    
    protected function mainContent() {
      $this->showTopFive($this->model->getTopFiveProducts());
    }
    
    protected function bodyHeader() {
      echo '<div class="jumbotron d-none d-md-block">'
          . '<h1>Producten top 5</h1>'.PHP_EOL
          . '</div>'.PHP_EOL;
    }


    private function showTopFive($productList){
      if($productList!=NULL){
        echo '<div class="container-fluid">'
        . '<h3>De bestverkochte producten van de afgelopen week zijn:</h3>'
        . '<table class="table table-striped">'
          . '<thead>'
            . '<th> Positie:</th>'
            . '<th> Product:</th>'
            . '<th> Aantal verkocht</th>'
          . '</thead>'
          . '<tbody>';
        foreach($productList as $key => $value){
          $this->displayTopItem($key, $value['name'], $value['amount'], $value['id']);
        }
        echo '</tbody>'
        . '</table>'
        . '</div>';
      } else{
        echo '<h3>Er zijn geen producten verkocht afgelopen week.</h3>';
      }
    }
    
    private function displayTopItem($position, $name, $amount, $id){
    echo '<tr>'.PHP_EOL
       . '<td>'.$position.'</td>'
       . '<td><a href="index.php?page=item&id='.$id.'">'.$name.'</a></td>'
       . '<td>'.$amount.'</td>'.PHP_EOL
       . '</tr>'.PHP_EOL;
  }
}
