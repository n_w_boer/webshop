<?php

class Util{
protected function getRequestedPage() /* JH TIP: Ik zou deze verplaatsen naar PageModel en daar direct de juiste properties laten zetten */
  {  
     $requested_type = $_SERVER['REQUEST_METHOD']; 
     if ($requested_type == 'POST') 
     { 
        $requested_page = $this->getPostVar('page','home'); 
     } 
     else 
     { 
       $requested_page = $this->getUrlVar('page','home'); 
     } 
     return $requested_page; 
  }  
//============================================== 
  protected function getArrayVar($array, $key, $default='') 
  { 
    return isset($array[$key]) ? $array[$key] : $default; 
  } 
//============================================== 
  protected function getPostVar($key, $default='') 
  { 
    $value = filter_input(INPUT_POST, $key); 
    return isset($value) ? $value : $default; 
  } 
//============================================== 
  protected function getUrlVar($key, $default='') 
  { 
    $value = filter_input(INPUT_GET, $key); 
    /* This is a modern variant of 
          return getArrayVal($_GET , $key, $default); or 
          return isset($_GET[$key]) ? $_GET[$key] : $default;
       see https://www.php.net/manual/en/function.filter-input.php 
       for extra options */
    return isset($value) ? $value : $default; 
  }   
  //=================================
  protected function correctData($data) { 
    return htmlspecialchars(stripslashes(trim($data)));
  }
}