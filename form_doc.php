<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Description of form_doc
 *
 * @author Gebruiker
 */
abstract class FormDoc extends BasicDoc{
            
    public function __construct($model) {
      parent::__construct($model);
    }
  protected function beginForm()
  {
    echo '<form action="index.php" method="post">'.PHP_EOL;
  }
  
  protected function hiddenFormInput($hiddenInput, $name = 'page')
  {
    echo '<input type="hidden" name="'.$name.'" value="'.$hiddenInput.'">'.PHP_EOL;
  }
  
  protected function showFormInput($id, $label, $type, $value='', $rows=5, $cols=15, $require="required", $min=1, $max=2, $labelWidth=3, $formWidth=7)
  {
    echo '<div class="form-group form-row">'
        .'<label class="col-'.$labelWidth.' col-form-label" for="'.$id.'">'.ucfirst($label).':</label>'.PHP_EOL
        .'<div class="col-'.$formWidth.'">';
    if(strcmp($type, 'textarea')==0){
      echo '<textarea id="'.$id.'" class="form-control" name="'.$id.'" rows="'.$rows.'" cols="'.$cols.'" placeholder="Your message:">'.$value.'</textarea>'.PHP_EOL;
    } else if(strcmp($type, 'number')==0&&strcmp($id, 'amount')==0){
        echo '<input type="'.$type.'" id="'.$id.'" class="form-control" name="'.$id. /* JH TIP: Zet de value ook op $min */'" min="'.$min.'" max="'.$max.'" required>'.PHP_EOL;
    } else {
      echo '<input type="'.$type.'" id="'.$id.'" class="form-control" name="'.$id.'" value="'.$value.'" '.$require.'>'.PHP_EOL;
    }
    echo '</div></div>'.PHP_EOL;
  }
  
  protected function formEnd($type, $label = 'Verzenden')
  {
    echo '<input class="btn btn-success" type="'.$type.'" value="'.$label.'">'. 
    '</form>'.PHP_EOL;
  }
  
  protected function showErrors(){
      if(!empty($this->model->getErrors())) {    
      echo '<p class="text-danger">Er zijn de volgende problemen opgetreden:<br>';
      foreach($this->model->getErrors() as $value) {
        echo $value.'<br>';
      }
      echo '</p>';
    }
  }
  
  protected abstract function formFields();
    
  public function mainContent(){
    $this->formFields();
  }
}
