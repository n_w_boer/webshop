<?php /* JH TIP: Zet de bestanden in folders /models, /controllers, /views etc */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'session_manager.php';
require_once 'util.php';
/**
 * Description of page_model
 *
 * @author Gebruiker
 */
class PageModel extends Util{
  protected $page;
  protected $menu;
  protected $loggedIn=false;
  protected $name='';
  protected $sessionmanager; /* JH TIP: gebruik camelCase dus sessionManager */
  private $cart=[];
  private $totalPrice;

  public function __construct($page /* JH TIP: Ik zou een kopie van $PageModel meegeven hier, zodat je niet opnieuw de loggedIn etc zet */) {
    $this->sessionmanager=new SessionManager();
    $this->page=$page;
    if($this->sessionmanager->loggedInUser()){
      $this->sumPrices(); /* JH: Dit hoort niet in een basis class als PageModel */
      $this->loggedIn=true;
      $this->name= $this->sessionmanager->getLoggedInUserName();
    }
  }
    
  public function getPage(){
    return $this->page;
  }
  
  public function setPage($page){
    $this->page=$page;
  }
    
  public function getMenu() {
    return $this->menu;
  }
    
  public function logOut(){
    $this->sessionmanager->logUserOut();
    $this->loggedIn=false;
    $this->name='';    
  }
    
  private function sumPrices(){ /* JH: Ik zou deze verplaatsen naar shopModel */
    $price = 0;
    if(array_key_exists('cart', $_SESSION)){
      $this->cart= $this->sessionmanager->cartInventory();
      foreach ($this->cart as $value){
        $price+=$value['price']*$value['amount']; /* JH TIP: Maak hier ook gebruik van objecten */
      }
      $this->totalPrice= $price/100;
    } 
  }
    
  public function isLoggedIn(){
    return $this->loggedIn;
  }
  
 
  public function getCart() {
    return $this->cart;    
  }
  
  public function getTotalPrice(){ /* JH: Ik zou deze verplaatsen naar shopModel */
    return $this->totalPrice;
  }
    
    
  public function createMenu(){
    $this->menu = [
      'home' => ['label' => 'Home', 'align' => 'left'],
      'about' => ['label' => 'About', 'align' => 'left'],
      'contact' => ['label' => 'Contact', 'align' => 'left'],
      'webshop' => ['label' => 'Webshop','align' => 'left'],
      'topfive' => ['label' => 'Producten top 5','align' => 'left'],];
    if ($this->loggedIn) {
      $this->menu['logout'] = ['label' => 'Uitloggen', 'align' => 'right', 'linkAddition' => ucfirst($this->name)];
      $this->menu['shoppingcart'] = ['label' => 'Winkelwagen', 'align' => 'right']; /* JH: Voeg hier de zaken toe om het submenu van de shopping cart te kunnen tonen
                                                                                           bijv: $this->menu['shoppingcart'] = ['label' => 'Winkelwagen', 'align' => 'right', 'cart' = getCart() ]]; */
      $this->menu['checkout'] = ['label' => 'Afrekenen', 'align' => 'right'];
    } else {
      $this->menu['login'] = ['label' => 'Log in', 'align' => 'right'];
      $this->menu['register'] = ['label' => 'Register', 'align' => 'right'];
    }
  }
}
