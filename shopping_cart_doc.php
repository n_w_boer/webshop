<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Shows the shopping cart Page
 *
 * @author Gebruiker
 */
class ShoppingCartDoc extends BasicDoc {
    public function __construct($model) {
      parent::__construct($model);
    }
    
    protected function mainContent() {
      $this->displayCart($this->model->getCart());
    }
    
  private function displayCart($cartItems){
    if(!empty($cartItems)){
      echo '<table class="table table-light table-striped my-2">'.PHP_EOL
      . '<tr>'
      . '<th>Product:</th>'
      . '<th>Aantal</th>'
      . '<th>Prijs per stuk</th>'
      . '<th>Totaalprijs</th>'
      . '</tr>'.PHP_EOL;
      foreach ($cartItems as $itemData){
        echo '<tr>';
        $this->displayCartItem($itemData);
        echo '</tr>';        
      }
      echo '</table>'.PHP_EOL
      . '<h2>De totaalprijs is: &euro; '.$this->model->getTotalPrice().'</h2>';
    }else{
      echo '<h2 class="text-warning">Uw mandje is leeg</h2>';
    }
  }
  
  private function displayCartItem($itemData){
    /* JH: In opdracht 3.1 staat dat je op de winkelwagen pagina het aantal van de bestelling moet kunnen veranderen, dat vind ik hier niet meer terug */
    echo '<td>'.$itemData['name'].'</td><td>'.$itemData['amount'].'</td><td>&euro;'.($itemData['price']/100).'</td><td>&euro;'.($itemData['price']*$itemData['amount']/100).'</td>';
  }
  
}
