<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
testTopFive(); /* JH: Je moet nu continue deze regel aanpassen om een andere script te testen, dat kan een geautomatiseerd process niet, het is beter om voor ieder script een eigen file te hebben */

function testTopFive(){
  include_once 'data_access_layer.php';
  include_once 'page_model.php';
  include_once 'shop_model.php';
  $testmodel = new PageModel('home');
  $model = new ShopModel($testmodel);
  $model->getFiveMostBought();
  $text=$model->getTopFiveProducts();
}

function testHtmlDoc(){
  include_once "html_doc.php";

  $data = array ( 'page' => 'home' );

  $view = new HtmlDoc($data);
  $view  -> show();
}

function testBasicDoc(){
  include_once "basic_doc.php";

  $data = array ( 'page' => 'home' );
  $data['menu']=createMenu();

  $view = new BasicDoc($data);
  $view  -> show();
}

function testAboutDoc(){
  include_once "about_doc.php";

  $data = array ( 'page' => 'about' );
  $data['menu']=createMenu();

  $view = new AboutDoc($data);
  $view  -> show();
}

function testHomeDoc(){
  include_once "home_doc.php";

  $data = array ( 'page' => 'home' );
  $data['menu']=createMenu();

  $view = new HomeDoc($data);
  $view  -> show();
}

function testContactDoc(){
  include_once "contact_doc.php";

  $data = array ( 'page' => 'contact' );
  $data['menu']=createMenu();

  $view = new ContactDoc($data);
  $view  -> show();
}

function testLoginDoc(){
  include_once "login_doc.php";

  $data = array ( 'page' => 'login' );
  $data['menu']=createMenu();

  $view = new LoginDoc($data);
  $view  -> show();
}

function testRegisterDoc(){
  include_once "register_doc.php";

  $data = array ( 'page' => 'registreren' );
  $data['menu']=createMenu();

  $view = new RegisterDoc($data);
  $view  -> show();
}

function testTopFiveDoc(){
  include_once "top_five_doc.php";

  $data = array ( 'page' => 'product top 5' );
  $data['menu']=createMenu();

  $view = new TopFiveDoc($data);
  $view  -> show();
}

function testShoppingCartDoc(){
  include_once "shopping_cart_doc.php";

  $data = array ( 'page' => 'Boodschappenmandje' );
  $data['menu']=createMenu();

  $view = new ShoppingCartDoc($data);
  $view  -> show();
}

function createMenu(){
    require_once 'user_repository.php';
    $array = array(
      'home' => array('label' => 'Home', 'align' => 'left'),
      'about' => array('label' => 'About', 'align' => 'left'),
      'contact' => array('label' => 'Contact', 'align' => 'left'),
      'webshop' => array('label' => 'Webshop','align' => 'left'),
      'topfive' => array('label' => 'Producten top 5','align' => 'left'),);
    return $array;
  }