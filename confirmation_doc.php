<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'basic_doc.php';
/**
 * Description of confirmation_doc
 *
 * @author Gebruiker
 */
class ConfirmationDoc extends BasicDoc{
  public function __construct($model) {
    parent::__construct($model);
  }
  
  protected function mainContent() {
    echo '<h2>Uw bestelling is gelukt:</h2>';
    foreach ($this->model->getCart() as $value){
      $this->displayShopList($value);
    }
  }
  
  private function displayShopList($item){
    echo '<div>'
    .'<p>'. $item['amount'].'x  <strong>'.$item['name'].'  </strong></p>'
     . '</div>';
  }
}
