<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'form_doc.php';
/**
 * Description of checkout_doc
 *
 * @author Gebruiker
 */
class CheckoutDoc extends FormDoc {
  public function __construct($model) {
    parent::__construct($model);
  }
    
  public function mainContent() {
    if(($this->model->getTotalPrice())>0){
      $this->displayCartItems($this->model->getCart());
      $this->formFields();
    } else{
      echo '<h2 class="text-warning">Uw mandje is leeg</h2>';
    }  
  }
    
  protected function formFields() {
    parent::showErrors();
    parent::beginForm();
    parent::hiddenFormInput('checkout');
    parent::showFormInput('street', 'Straat', 'text', $this->model->getStreet());
    parent::showFormInput('nr', 'Huisnummer', 'number', $this->model->getNr());     
    parent::showFormInput('nr_addition', 'toevoeging', 'text', $this->model->getNrAddition(),0,0,'');
    parent::showFormInput('zipcode', 'Postcode', 'text', $this->model->getZipcode());
    parent::showFormInput('city', 'Stad', 'text', $this->model->getCity());
    parent::formEnd('submit', 'Afrekenen');
  }
    
    private function displayCartItems($cartItems){
    if(!empty($cartItems)){
      echo '<table class="table table-dark table-striped my-2">'.PHP_EOL
      . '<tr>'
      . '<th>Product:</th>'
      . '<th>Aantal</th>'
      . '<th>Prijs per stuk</th>'
      . '<th>Totaalprijs</th>'
      . '</tr>'.PHP_EOL;
      foreach ($cartItems as $itemData){
        echo '<tr>';
        $this->displayCartItem($itemData);
        echo '</tr>';        
      }
      /* JH: Mis hier het totaalbedrag van de hele bestelling */
      echo '</table>';
    }
  }
  
  private function displayCartItem($itemData){
    echo '<td>'.$itemData['name'].'</td><td>'.$itemData['amount'].'</td><td>&euro;'.($itemData['price']/100).'</td><td>&euro;'.($itemData['price']*$itemData['amount']/100) /*JH: Formateer bedragen altijd in 2 decimalen */.'</td>';
  } 

}
