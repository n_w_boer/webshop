<?php /* JH TIP: Zet de bestanden in folders /models, /controllers, /views etc */
 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'util.php';
/**
 * Description of page_controller
 *
 * @author Gebruiker
 */
class PageController extends Util /* JHL: Als je getRequestedPage een functie van PageModel laat zijn hoeft deze extends niet */ {
    protected $model;
    
    public function __construct() {
      $this->model=new PageModel($this->getRequestedPage());
    }
    
    public function processRequest() 
  { 
    switch ($this->model->getPage()) 
    {  
       case 'contact':
           require_once 'user_controller.php';
           $userController = new UserController();
           $this->model = $userController->processContact(); /* JH: Als je eenmaal de controle overgeeft aan een andere controller, zal die ook de view maken en laten zien */
           break;
       case 'login':
           require_once 'user_controller.php';
           $userController = new UserController();
           $this->model =$userController->processLogin();
           break;
       case 'register':
           require_once 'user_controller.php';
           $userController = new UserController();
           $this->model =$userController->processRegister();
           break;
       case 'checkout':
           require_once "user_controller.php";
           $userController = new UserController();
           $this->model = $userController->processCheckout();
           break;
       case 'logout':
           $this->processLogout();
           break;
       case 'item':
           require_once "shop_controller.php";
           $shopController = new ShopController($this->model);
           $this->model = $shopController->processItem();
           break;
       case 'webshop':
           require_once "shop_controller.php";
           $shopController = new ShopController($this->model);
           $this->model = $shopController->processWebshop();
           break;
       case 'shoppingcart':
           require_once "shop_controller.php";
           $shopController = new ShopController($this->model);
           $this->model = $shopController->processShoppingCart();
           break;
       case 'topfive':
           require_once "shop_controller.php";
           $shopController = new ShopController($this->model);
           $this->model = $shopController->processTopFive();
           break;
       default: 
           break; 
    }
    $this->model->createMenu();
  }  
  
  public function showResponsePage() 
  { 
    switch ($this->model->getPage()) 
    { 
       case 'home':
           require_once "home_doc.php";
           $view = new HomeDoc($this->model);
           break; 
       case 'about': 
           require_once "about_doc.php";
           $view = new AboutDoc($this->model);
           break; 
       case 'contact': 
           require_once "contact_doc.php";
           $view = new ContactDoc($this->model);
           break;  
       case 'thanks':
           $view = new ThanksDoc($this->model);
           break;
       case 'login':
           require_once "login_doc.php";
           $view = new LoginDoc($this->model);
           break;
       case 'register':
           require_once "register_doc.php";
           $view = new RegisterDoc($this->model);
           break;
       case 'topfive':
           require_once 'top_five_doc.php';
           $view = new TopFiveDoc($this->model);
           break;
       case 'webshop':
           require_once "webshop_doc.php";
           $view = new WebshopDoc($this->model);
           break;
       case 'item':
           require_once 'item_doc.php';
           $view = new ItemDoc($this->model);
           break;
       case 'shoppingcart':
           require_once 'shopping_cart_doc.php';
           $view = new ShoppingCartDoc($this->model);
           break;
       case 'checkout':
           require_once 'checkout_doc.php';
           $view = new CheckoutDoc($this->model);
           break;
       case 'confirm';
           require_once 'confirmation_doc.php';
           $view = new ConfirmationDoc($this->model);
           break;
       default: 
           /* JH: Mis de require_once "home_doc.php"; hier */
           $view = new HomeDoc($model);
           break; 
    }
    $view->show();
  }
  
   //================================
  function processLogout(){ /* JH TIP: Ik zou dit in een functie logoutUser in het model doen. */
    $this->model->setPage('home');
    $this->model->logOut();
    $this->model->createMenu();
  }

  
  
  
}
