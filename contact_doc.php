<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'form_doc.php';
/**
 * Description of contact_doc
 *
 * @author Gebruiker
 */
class ContactDoc extends FormDoc {
    public function __construct($model) {
        parent::__construct($model);
    }
    
    protected function bodyHeader() {
      if($this->model->isValid()){
        echo '<div class="jumbotron d-none d-md-block">'
          . '<h1>Thanks</h1>'.PHP_EOL
          . '</div>'.PHP_EOL;
      }else{
        echo '<div class="jumbotron d-none d-md-block">'
          . '<h1>'.ucfirst($this->model->getPage()).'</h1>'.PHP_EOL
          . '</div>'.PHP_EOL;
      }
    }
    public function mainContent(){
      if($this->model->isValid()){
        echo '<p>Bedankt voor uw reactie:</p>'
      . '<div>Naam: '.$this->model->getName().'</div>'
      . '<div>Email: '.$this->model->getEmail().'</div>'
      . '<div>Uw bericht: '.$this->model->getMessage().'</div>';
      }else{
        $this->formFields();
      }
    }
    
    protected function formFields() {
      parent::showErrors($this->model->getErrors());
      parent::beginForm();
      parent::hiddenFormInput('contact', 'page');
      parent::showFormInput('name', 'Naam', 'text',  $this->model->getName());
      parent::showFormInput('email', 'Email', 'email', $this->model->getEmail());
      parent::showFormInput('message', 'Bericht', 'textarea', $this->model->getMessage(), 5, 30);
      parent::formEnd('submit', 'Verzenden');
    }
    
}
