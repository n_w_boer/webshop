<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'data_access_layer.php';

/**
 * Description of data_model
 *
 * @author Gebruiker
 */
class DataModel extends PageModel{
  protected $dataAccessLayer;
  private $dataBaseError;
  
  public function __construct($page) {
    parent::__construct($page);
    $this->dataAccessLayer=new DataAccessLayer();
    $this->dataBaseError=false;
  }
  
  public function errorBool(){ /* JH: Vreemde functienaam, noem hem hasErrors ofzo */
    return $this->dataBaseError;
  }
  
  public function checkExceptions(){ /* JH: Zie opmerking over het vangen van excepties in data_access_layer */
    return $this->dataAccessLayer->getError();
  }
  
  public function clearDatabaseErrors() {
    $this->dataAccessLayer->clearError();
  }
}
