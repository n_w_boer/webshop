<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'form_doc.php';
/**
 * Description of contact_doc
 *
 * @author Gebruiker
 */
class LoginDoc extends FormDoc {
    public function __construct($model) {
        parent::__construct($model);
    }
    
    protected function formFields() {
      parent::showErrors($this->model->getErrors());
      parent::beginForm();
      parent::hiddenFormInput('login', 'page');
      parent::showFormInput('email', 'Email', 'email', $this->model->getEmail());
      parent::showFormInput('password', 'Wachtwoord', 'password');
      parent::formEnd('submit', 'Login');
    }
    
}
