<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlDoc
 *
 * @author Gebruiker
 */
class HtmlDoc {
  private function beginDoc()  
  {  
    echo "<!DOCTYPE html>\n<html>\n";  
  } 

  private function beginHeader()  
  {  
    echo "<head>\n";  
  }

  protected function headerContent()
  {
    echo "<title>Mijn eerste class</title>\n";
  }
      
  private function endHeader() 
  {  
    echo "</head>\n";  
  } 
        
  private function beginBody()  
  {  
    echo '<body class="content">'.PHP_EOL
        .'<div class="container-fluid">';  
  } 
        
  protected function bodyContent()  
  {  
    echo "<h1>Mijn eerste class</h1>\n";  
  } 
       
  private function endBody()  
  {  
    echo "</div></body>\n";  
  } 
       
  private function endDoc()  
  {  
    echo "</html>\n";  
  } 
   
  public    function show() 
  { 
    $this->beginDoc(); 
    $this->beginHeader(); 
    $this->headerContent(); 
    $this->endHeader(); 
    $this->beginBody(); 
    $this->bodyContent(); 
    $this->endBody(); 
    $this->endDoc(); 
  }
}
