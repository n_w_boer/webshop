<?php /* JH TIP: Zet de bestanden in folders /models, /controllers, /views etc */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'data_model.php';
/**
 * Description of shop_model
 *
 * @author Gebruiker
 */
class ShopModel extends DataModel {
  private $productList;
  private $itemData;
  private $topFiveProducts;
  private $allowedToBuy;

  public function __construct($model) {
    parent::__construct($model->getPage());
    $this->allowedToBuy=$model->isLoggedIn();
  }
  
  public function setItemData($product_id){
    $this->itemData = $this->dataAccessLayer->findAllXbyKeyInY($product_id, 'product_id', 'products');
    /* JH: Mis hier een check als itemData leeg is (bijv. als iemand een onbekend product opvraagt) */
    $this->itemData['max']= $this->getMax($product_id, $this->itemData['stock']); /* JH: Deze code lijkt gedupliceerd op regel 70, maak er een functie van */
    $this->itemData['min']= $this->getMin($product_id, $this->itemData['stock']);
  }
  
  public function getItemData(){
    return $this->itemData;
  }
  
  protected function findItemData($product_id){
    $itemData = $this->dataAccessLayer->findAllXbyKeyInY($product_id, 'product_id', 'products');
    return $itemData;
  }
  
  public function fillCart($id, $amount) {
    if(array_key_exists($id, $this->getCart())){
      $this->sessionmanager->increaseAmount($id, $amount);
    }else{
      $item=$this->findItemData($id);
      /* JH: DEFENSIEF programmeren, Wat als hier geen valide item uit komt? */
      $this->sessionmanager->addToCart($id, $item['name'], $amount, $item['price']);
    }
  }
  
  public function getFiveMostBought(){
    $this->topFiveProducts= $this->dataAccessLayer->getTopFive();
  }
  
  public function getTopFiveProducts(){
    return $this->topFiveProducts;
  }
  
  public function getProductList(){
    return $this->productList;
  }
  
  public function setProductList(){
    $this->productList=$this->dataAccessLayer->listOfProducts();
    $this->addMinMax(); /* JH TIP: Je kan de foreach ook gelijk hierin zetten, in plaats van een losse functie */
  }
  
  public function addMinMax(){
    foreach ($this->productList as $key=>$value){
      $this->productList[$key]['min']= $this->getMin($key, $value['stock']);
      $this->productList[$key]['max']= $this->getMax($key, $value['stock']);
    }
  }
  
  private function getMin($id, $stock){
    if(!empty($this->cart)&& array_key_exists($id, $this->cart)){
      return 1-$this->cart[$id]['amount'];
    }else {
      return 1;
    }
  }
  
  private function getMax($id, $stock){
    if(!empty($this->cart)&& array_key_exists($id, $this->cart)){
      return $stock-$this->cart[$id]['amount'];
    }else {
      return $stock;
    }
  }
 
  public function isAllowedToBuy(){
    return $this->allowedToBuy;
  }
  
  
}
