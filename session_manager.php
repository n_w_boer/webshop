<?php
  
  //=============================
  //FUNCTIONS
  //==============================
class SessionManager{

  function startSession(){
    session_start();
  }
  //==============================
  function logUserOut(){
    unset($_SESSION['name']);
    unset($_SESSION['cart']);
  }
  //===============================
  function loggedInUser(){
    return isset($_SESSION['name']);
  }
  //===============================
  function addToCart($product_id, $name, $amount, $price){
    $_SESSION['cart'][$product_id] = ['name'=>$name, 'amount'=>$amount, 'price'=>$price];
  }
  //===============================
  function getLoggedInUserName(){
    return $_SESSION['name'];
  }
  //==============================
  function getLoggedInID(){
    return $_SESSION['customer_id'];
  }
  //==============================
  function logUserIn($name, $id)
  {
    $_SESSION['name']=$name;
    $_SESSION['customer_id']=$id;
    $_SESSION['cart'] = [];
  }
  //===============================
  function unsetCart(){
    unset($_SESSION['cart']);
  }
  //===============================
  function cartInventory(){
    return $_SESSION['cart'];
  }
  //=============================
  function inCart($product_id){
    return key_exists($product_id, $_SESSION['cart']);
  }
  //=============================
  function increaseAmount($product_id, $amount){
    $_SESSION['cart'][$product_id]['amount']+=$amount;
  }
  //=============================
  function amount($id){
    return $_SESSION['cart'][$id]['amount'];
  }
  //=============================
  function updateCartItem($array, $key){
    if (empty($key)||$array['amount']==0){
      unset($_SESSION['cart'][$key]);
    }
  }
  //=============================
  function makeCartEmpty(){
    $_SESSION['cart'] = [];
  }
  //=============================
  function isCartEmpty(){
    return array_key_exists('cart', $_SESSION);
  }
  //=============================
  public function updateCart($cart){
    foreach($cart as $key =>$value){
      $this->updateCartItem($value, $key);
    }
  }
}