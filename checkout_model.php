<?php /* JH TIP: Zet de bestanden in folders /models, /controllers, /views etc */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'user_model.php';
/**
 * Description of checkout_controller
 *
 * @author Gebruiker
 */
class CheckoutModel extends UserModel{
  private $street='';
  private $nr=1;
  private $nr_addition='';
  private $zipcode='';
  private $city='';
  
  public function __construct($pageModel) {
    parent::__construct($pageModel);
  }
  
  public function getStreet(){
    return $this->street;
  }
  
  public function getNr(){
    return $this->nr;
  }
  
  public function getNrAddition(){
    return $this->nr_addition;
  }
  
  public function getZipcode(){
    return $this->zipcode;
  }
  
  public function getCity(){
    return $this->city;
  }
  
  private function getStock($id){
    return $this->dataAccessLayer->getXfromYbyNumKey($id, 'stock', 'product_id', 'products');
  }
  
  public function validateCheckout(){
    $this->sessionmanager->updateCart($this->getCart());
    $this->street = $this->getData("street");
    $this->nr = $this->getData("nr");
    $this->nr_addition = $this->getData("nr_addition");
    $this->zipcode = $this->getData("zipcode");
    $this->city = $this->getData("city");
    foreach ($this->getCart() as $key =>$value){
      if($this->isEmpty($key)|| $this->isEmpty($value['name'])|| $this->isEmpty($value['amount'])|| $this->isEmpty($value['price'])){
        $this->errors['cart']="nietbestaand item in mandje";
      }else if($this->getStock($key)<$value['amount']){ /* JH: Hier wordt een query gedaan voor elke regel in het mandje, dat is niet gewenst, doe vooraf 1 query om alle stock van alle producten in het mandje op te vragen */
        $this->errors['amount'].="meer ".$value['name']." besteld dan op voorraad.<br>";
      }
    }
    if(!$this->isEmpty($this->checkExceptions())){
      $this->errors['database']= $this->checkExceptions();
      $this->clearDatabaseErrors();
    }
    if (empty($array['error'])) {
      $data = $this->validateAddress();
      if($this->isEmpty($this->errors)){
        $this->valid=true;
      }
    }
  }
  //=================================
  private function validateAddress(){
      if($this->isEmpty($this->street)){
        $this->errors['street'] = "Vul je straat in.";
      }else if(!$this->checkStreet($this->street)){
        $this->errors['street'] = "Je straatnaam kan niet bestaan uit cijfers en interpunctie.";
      }
      if($this->isEmpty($this->nr)){
        $this->errors['nr'] = "Vul je huisnummer in.";
      }else if (!$this->checkNr($this->nr)) {
        $this->errors['nr'] = "Verkeerd huisnummer format";
      } 
      if ($this->isEmpty($this->zipcode)) {
        $this->errors['zipcode'] = "postcode is verplicht";
      } else if (!$this->checkZipcode($this->zipcode)){
        $this->errors['zipcode'] = "postcode staat niet in de juiste vorm (1234 AB)";
          
      }
      if ($this->isEmpty($this->city)) {
        $this->errors['city'] = "Stad is verplicht";
      } else if(!$this->checkLettersAndSpaces($this->city)){
        $this->errors['city'] = "De naam van de stad waar je woont kan niet bestaan uit cijfers en interpunctie..";
      }
  }  
  //=================================
  public function confirmOrder(){
    $adressId = $this->storeAdress($this->street, $this->nr, $this->nr_addition, $this->zipcode, $this->city);
    $this->storeOrder($adressId);
    $this->sessionmanager->makeCartEmpty();    
  }
  //=================================
  private function storeAdress($street,$nr,$nr_addition,$zipcode,$city){
    return $this->dataAccessLayer->saveAdress($street,$nr,$nr_addition,$zipcode,$city);
  }
  //=================================
  private function storeOrder($adress_id){
    $orderID = $this->dataAccessLayer->saveOrder($this->sessionmanager->getLoggedInID(), $adress_id);
    $this->addOrderProducts($orderID);
  }
  //=================================
  private function addOrderProducts($orderID){
    $orderData = $this->getCart();
    $dateInfo = getdate();
    if($dateInfo['mon']<10){ /* JH TIP: Meestal hou je een date een date-object tot in de data-access layer */
      $date = $dateInfo['year'].'-0'.$dateInfo['mon'].'-'.$dateInfo['mday'];
    } else{
      $date = $dateInfo['year'].'-'.$dateInfo['mon'].'-'.$dateInfo['mday'];        
    }
    foreach ($orderData as $key => $value) {
      $this->dataAccessLayer->saveOrderProduct($date, $orderID, $key, $value['amount'], $value['price']);
      $this->dataAccessLayer->updateStock($key,$value['amount']);
    }     
  }


  //=================================
  private function checkStreet($street){
    return preg_match("/^[a-zA-Z0-9.\- ]*$/",$street); 
  }
  //=================================
  private function checkZipCode($var){
    return preg_match("/^\d{4}\s?\w{2}$/",$var);
  }
  //=================================
  private function checkNr($nr){
    return preg_match("/^\d*$/", $nr);
  }
  //=================================

}
