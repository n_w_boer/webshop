-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 24, 2020 at 03:29 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nielsb_website_users`
--

-- --------------------------------------------------------

--
-- Table structure for table `adress`
--

CREATE TABLE `adress` (
  `adress_id` int(10) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(10) NOT NULL,
  `addition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adress`
--

INSERT INTO `adress` (`adress_id`, `street`, `number`, `addition`, `zipcode`, `city`) VALUES
(21, 'tset', 34, 'a', '1234ab', 'sfasa'),
(22, 'tsett', 53, 'v', '1423ab', 'stat'),
(23, 'test', 35, 'test', '1234AB', 'test'),
(24, 'test', 123, 'abc', '1234rf', 'adfasfa'),
(25, 'werwer', 4, '2', '1234aa', 'ddgfdg'),
(26, 'destraat', 35, 'bis a', '3521aa', 'utrecht');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(10) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `email`, `name`, `password`) VALUES
(1, 'boertjeniels@gmail.com', 'Niels', 'test'),
(2, 'test@gmail.com', 'Test', 'tes'),
(3, 'vader123@test.nl', 'De vader', '123');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(10) NOT NULL,
  `customer_id_fk` int(10) NOT NULL,
  `adress_id_fk` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `customer_id_fk`, `adress_id_fk`) VALUES
(6, 1, 21),
(7, 1, 22),
(8, 1, 23),
(9, 1, 23),
(10, 3, 25),
(11, 3, 26);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `orderproducts_id` int(10) NOT NULL,
  `order_date` date NOT NULL,
  `order_id_fk` int(10) NOT NULL,
  `product_id_fk` int(10) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`orderproducts_id`, `order_date`, `order_id_fk`, `product_id_fk`, `amount`, `price`) VALUES
(10, '2020-03-24', 6, 1, 3, '4.99'),
(11, '2020-03-24', 6, 3, 2, '9.99'),
(12, '2020-03-24', 6, 4, 2, '22.49'),
(13, '2020-03-24', 6, 5, 2, '4.99'),
(14, '2020-03-24', 6, 6, 1, '49.99'),
(15, '2020-03-24', 6, 8, 2, '39.99'),
(16, '2020-03-24', 6, 7, 2, '14.99'),
(17, '2020-03-24', 7, 7, 4, '14.99'),
(18, '2020-03-24', 7, 5, 7, '4.99'),
(19, '2020-03-24', 7, 1, 2, '4.99'),
(20, '2020-03-24', 7, 6, 2, '49.99'),
(21, '2020-03-24', 7, 8, 7, '39.99'),
(22, '2020-03-24', 8, 1, 1, '4.99'),
(23, '2020-03-24', 8, 3, 3, '9.99'),
(24, '2020-03-24', 8, 6, 2, '49.99'),
(25, '2020-03-24', 8, 8, 6, '39.99'),
(26, '2020-03-24', 10, 8, 45, '39.99'),
(27, '2020-03-24', 10, 1, 125, '4.99'),
(28, '2020-03-24', 11, 8, 7, '39.99'),
(29, '2020-03-24', 11, 1, 4, '4.99');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stock` int(10) NOT NULL,
  `image_src` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `price`, `stock`, `image_src`, `description`) VALUES
(1, 'scheerlijnenvet (150g)', '4.99', 1844, 'scheerlijnenvet.png', 'Een onmisbaar onderdeel van elke tent zijn natuurlijk de scheerlijnen, en om te voorkomen dat deze slijten moet je deze eens per maand goed vetten met scheerlijnvet, tegendraads insmeren en goed in laten trekken.'),
(2, 'scheerlijnenvet (500g)', '12.49', 0, 'scheerlijnenvet.png', 'Een onmisbaar onderdeel van elke tent zijn natuurlijk de scheerlijnen, en om te voorkomen dat deze slijten moet je deze eens per maand goed vetten met scheerlijnvet, tegendraads insmeren en goed in laten trekken. Deze grote hoeveelheid zorgt ervoor dat u de komende tijd geen nieuwe hoeft te halen'),
(3, 'rubber haringen (20 stuks)', '9.99', 447, 'rubberharingen.jpg', 'Om je tent goed vast te zetten heb je haringen nodig. Onze a-merk rubberen haringen gaan extra lang mee en als ze verbuigen is dat geen probleem want ze blijven bruikbaar.'),
(4, 'rubber haringen (50 stuks)', '22.49', 488, 'rubberharingen.jpg', 'Om je tent goed vast te zetten heb je haringen nodig. Onze a-merk rubberen haringen gaan extra lang mee en als ze verbuigen is dat geen probleem want ze blijven bruikbaar.'),
(5, 'rode haringen (10 stuks)', '4.99', 990, 'rode_haringen.jpg', 'Soms dan heb je gewoon een geweldige afleiding nodig, gebruik daarom onze A merk rode haringen. Met een enkele kan je een spooktocht al laten slagen, en met 10 stuks kan je je thema het hele kamp spannend houden.'),
(6, 'bosstofzuiger', '49.99', 13, 'bosstofzuiger.jpg', 'Ben jij nou ook klaar met al die dennenappels waar je je tent neer wilt zetten? En de steentjes en takjes die je luchtbed kapotprikken? Koop dan nu een bosstofzuiger, maakt gegarandeerd je kampeerplaats tak, steen, en dennenappel vrij.'),
(7, 'Middenpaal verkennertent', '14.99', 195, 'middenpaal.jpg', 'Ben jij er ook helemaal klaar mee dat je tentpaal uit je tent gestolen wordt terwijl je slaapt? Koop dan nu een extra middenpaal en beëindig het wanbeleid van de oudere groepen zodat je \'s ochtends niet van alles hoeft te doen om hem van ze terug te krijgen.'),
(8, 'schop met \'t voetje', '39.99', 321, 'schop.jpg', 'Wordt er nooit meer op uitgestuurd om de schop van \'t voetje te halen. met deze kwaliteits schep kan je nog wel even vooruit en door de ergonomische grip heb je een stuk minder last van spierpijn.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adress`
--
ALTER TABLE `adress`
  ADD PRIMARY KEY (`adress_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`orderproducts_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adress`
--
ALTER TABLE `adress`
  MODIFY `adress_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `orderproducts_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
