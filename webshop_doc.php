<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'form_doc.php';
/**
 * Description of webshop_doc
 *
 * @author Gebruiker
 */
class WebshopDoc extends FormDoc{
    //private $itemID;
    private $itemData;
    
    public function __construct($model) {
        parent::__construct($model);
    }
    
    public function mainContent() {
      $this->displayWebshop($this->model->getProductList());
    }
    
    protected function formFields() {
      $data= $this->itemData;
      $min = $this->model->getProductList()[$data['product_id']]['min'];
      $max = $this->model->getProductList()[$data['product_id']]['max'];
      parent::beginForm();      
      parent::hiddenFormInput('webshop', 'page');
      parent::hiddenFormInput($data['product_id'], 'product_id');
      parent::showFormInput('amount', 'aantal', 'number',1,0,0,'required',$min, $max, 6, 6);
      parent::formEnd('submit', 'Bestel');
    }
    
    private function displayWebshop($dataArray){
      echo '<div class="container">'
        . '<div class="row">'.PHP_EOL;
      foreach($dataArray as $key => $value){
        $this->displayWebshopItem($key, $value); 
      }
      echo '</div>'
      . '</div>';
  }
  
  private function displayWebshopItem($item_id, $value){
      $this->beginShopItem($item_id);
      $this->showImage($value['image_src']);
      echo '<div class="card-body">';
      $this->showItemName($value['name']);
      $this->showPriceAndOrder($item_id, $value);
      $this->endShopItem();      
  }
  
  private function beginShopItem($item_id){
    echo '<div class="card my-2 col-md-4 col-sm-6 col-lg-3 bg-light">'
    . '<a href="index.php?page=item&amp;id='.$item_id.'">'.PHP_EOL
    . '<div class="container">';
  }
  
  private function showImage($image_src){
    echo '<img class="img-fluid" src="images/'.$image_src.'">';
  }
  
  private function showItemName($name){
    echo '<h6>'.$name.'</h6>'.PHP_EOL;
  }
  
  private function showPriceAndOrder($id, $data){
    $this->showPrice($data['price']);
    echo '</div>';
    if($this->model->isLoggedIn()){
      echo '<div class="card-footer d-none d-md-block">';
      $this->showOrder($id, $data);
      echo '</div>';
    }
  }
  
  private function showOrder($item_id, $data){
    if(($this->model->getProductList()[$item_id]['max'])>0){
      $this->itemData=$data;
      $this->formFields();
    }else{
      echo 'Niet op voorraad';
    }
  }
  private function showPrice($price){
      echo '&euro;'.($price/100);
  }
  
  private function endShopItem(){
    echo '</div>'
    . '</a>'
    . '</div>';
  }
  

}
