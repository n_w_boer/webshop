<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BasicDoc
 *
 * @author Gebruiker
 */
require_once 'html_doc.php';
   
class BasicDoc extends HtmlDoc 
{ 
  protected $model;
  public function __construct($model)
  { 
    $this->model = $model;
  }       

  protected function title() 
  {
    echo "<title>Opdracht_4.2 - " . ucfirst($this->model->getPage()) . "</title>";
  } 
        
  private function metaData() 
  {
    echo '<meta charset="UTF-8">'.PHP_EOL;
    echo '<meta name="viewport" content="width=device-width, initial-scale=1">'.PHP_EOL;
    echo '<meta name = "author" content = "Niels Boer">'.PHP_EOL;
  } 
       
  private function links()
  {
    echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">'
    .'<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>'.PHP_EOL
    .'<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>'.PHP_EOL
    .'<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>'.PHP_EOL
    .'<link rel="icon" href="images/icon.jpg" type="image" sizes="16x16">'.PHP_EOL;
  }
        
  protected function bodyHeader()  
  {
    echo '<div class="jumbotron d-none d-md-block">'
    . '<h1>'.ucfirst($this->model->getPage()).'</h1>'.PHP_EOL
    . '</div>'.PHP_EOL;
  } 
        
  private function mainMenu() 
  {
    echo '<nav class="navbar navbar-expand-md bg-success">'
    . '<button class="navbar-toggler" data-toggle="collapse" type="button" data-target="#collapsibleNav">'
    . 'Menu<span class="navbar-toggler-item"></span></button>'
    . '<div class="collapse collapsible navbar-collapse"  id="collapsibleNav">'
    . '<ul class="navbar-nav nav-pills">'.PHP_EOL;
    foreach($this->model->getMenu() as $link => $menuItemArray) {
      if(strcmp($link, 'shoppingcart')==0){ /* JH: Het is niet de bedoeling om 'business' code in de presentatie logica te zetten, maak menuItemArray zodat het ook de data voor de cart bevat */
        $this->showItemBasket($link, $menuItemArray, $this->model->getPage());
      }else{
        $this->showMenuItem($link, $menuItemArray, $this->model->getPage());
      }
    }
    echo '</ul>'
    .'</div>'
    . '</nav>';
  } 
        
  private function showMenuItem($linkName, $menuItemArray, $page)
  {
    $active='';
    if(strcmp($linkName,$page)==0){
      $active='active';
    }
    if(!empty($menuItemArray['linkAddition'])){
      echo '<li class="nav-item"><a class="nav-link " href="index.php?page='.$linkName.'">'.ucfirst($menuItemArray['label']).'<i class="d-none d-lg-inline">: '.$menuItemArray['linkAddition'].'</i></a></li>'.PHP_EOL;    
    } else {
      echo '<li class="nav-item">'.$this->showMenuLink($linkName, $menuItemArray, $active).'</li>'.PHP_EOL;
    }
  }
  
  private function showMenuLink($linkName, $menuItemArray, $active=''){
    echo '<a class="nav-link '.$active.'" href="index.php?page='.$linkName.'">'.ucfirst($menuItemArray['label']).'</a>';
  }


  //=================================
  private function showItemBasket($linkName, $menuItemArray, $page){
      echo '<li class="nav-item dropdown bg-success">'.
        '<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">'.
        'Winkelwagen<span class="fa fa-shopping-cart d-none d-lg-inline"></span>'.
        '</a>'.PHP_EOL.
        '<div class="dropdown-menu">';
      $cart = $this->model->getCart();
      if(!empty($cart)){
        $this->showItemBasketDropdown($cart, $linkName, $menuItemArray, $page);
      }else{
        echo 'Uw mandje is leeg';
      }
      echo '</div>'.
      '</li>'.PHP_EOL;
  }
  //=================================
  private function showItemBasketDropdown($cart, $linkName, $menuItemArray, $page){
    echo '<table class="table table-sm">'.PHP_EOL.
      '<tr>'. 
        '<th>product</th>'.
        '<th>aantal</th>'.
        '<th>prijs</th>'.
      '</tr>'.PHP_EOL;    
    $this->displayDropdownCartItems($cart);
    echo '</table>';
    $menuItemArray['label']='Ga naar winkelwagen';
    $this->showMenuLink($linkName, $menuItemArray, $page);
  }
  //=================================
  private function displayDropdownCartItems($cart){
      foreach($cart as $key => $value){
      echo '<tr>';
      $this->displayDropdownCartItem($value['name'], $value['amount'], $value['price']/100);
      echo '</tr>'.PHP_EOL;
    }
  }
  //=================================
  private function displayDropdownCartItem($name, $amount, $price){
    echo '<td>'.$name.'</td>'.
         '<td>'.$amount.'</td>'.
         '<td>&euro;'.$price.'</td>'.PHP_EOL;
  } 
        
  protected function mainContent() {} 
       
  private function bodyFooter()
  {
    echo '<footer class="footer-copyright bg-dark text-white text-md-right">&copy; Niels Wouter Boer 2020</footer>';
  } 

  // Override function from htmlDoc
  protected function headerContent() 
  {
    $this->title();
    $this->metaData();
    $this->links();
  } 

  // Override function from htmlDoc
  protected function bodyContent() 
  {
    $this->bodyHeader();
    $this->mainMenu();
    $this->mainContent();
    $this->bodyFooter();
  }   
}
